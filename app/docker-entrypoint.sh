#!/bin/bash

# ln -sf /proc/self/fd/1 /formr/tmp/logs/errors.log
ln -sf /proc/self/fd/1 /var/log/apache2/access.log
ln -sf /proc/self/fd/1 /var/log/apache2/error.log

# if [ ! -f "/etc/formr/apache.conf" ]
# then
#     cp -r /config-examples/. /etc/formr/
# fi

# if [ ! -f "/etc/apache2/sites-available/formr.conf" ]
# then
#     ln -s /etc/formr/${FORMR_APACHE_CONFIG} /etc/apache2/sites-available/formr.conf
# fi

# a2dissite 000-default
# a2ensite formr
# a2enmod rewrite headers ssl xsendfile proxy proxy_http proxy_balancer lbmethod_byrequests

/usr/sbin/apache2ctl stop

/usr/sbin/apache2ctl -D FOREGROUND