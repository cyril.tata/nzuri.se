#!/bin/bash

# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install -y ca-certificates curl gnupg
sudo install  -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
sudo echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
sudo apt-get install -y docker-compose
sudo apt-get install -y git


sudo mkdir -p /apps/nzuri.se
sudo cd /apps/nzuri.se
sudo git clone https://gitlab.com/cyril.tata/nzuri.se.git .

sudo touch .env
sudo touch .apache.env

sudo echo "# .env \n" > .env

PASSWORD=$(date +%s|sha256sum|base64|head -c 32)
sudo echo "# mysql connection parameters" >> .env
sudo echo "MYSQL_ROOT_PASSWORD=$PASSWORD" >> .env
sudo echo "MYSQL_DATABASE=nzuri" >> .env
sudo echo "MYSQL_USER=nzuri" >> .env
sudo echo "MYSQL_PASSWORD=$PASSWORD" >> .env
sudo echo "\n\n"

sudo docker-compose up -d
